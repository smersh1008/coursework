package by.gsu.coursework;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Romanyuk Dmitriy.
 */
public class SchoolSchedule implements ActionListener {

    private JButton doButton;
    private JButton classesButton;
    private JButton teachersButton;
    private JTable table;
    private JList classesList;
    private JList teachersList;
    private JPanel mainPanel;
    private JPanel panel;
    private String pathClasses;
    private String pathTeachers;

    public static void main(String[] args) {
        SchoolSchedule schoolSchedule = new SchoolSchedule();
        JFrame frame = new JFrame("Школьное расписание");
        frame.setContentPane(new SchoolSchedule().mainPanel);
       // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        //schoolSchedule.gui();

    }

    private SchoolSchedule() {
        doButton.addActionListener(this);

        table.setModel(new DefaultTableModel(50, 50));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        classesButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(".");
                int returnValue = fileChooser.showOpenDialog(mainPanel);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        List<String> lines = Files.readAllLines(Paths.get(selectedFile.toURI()));
                        pathClasses = String.valueOf(Paths.get(selectedFile.toURI()));
                        classesList.setListData(lines.toArray(new String[lines.size()]));

                    } catch (IOException e1) {
                        e1.printStackTrace();

                    }

                }
            }
        });

        teachersButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(".");
                int returnValue = fileChooser.showOpenDialog(mainPanel);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        List<String> lines = Files.readAllLines(Paths.get(selectedFile.toURI()));
                        pathTeachers = String.valueOf(Paths.get(selectedFile.toURI()));
                        teachersList.setListData(lines.toArray(new String[lines.size()]));

                    } catch (IOException e1) {
                        e1.printStackTrace();

                    }

                }
            }
        });
    }

    public void actionPerformed(ActionEvent e) {
        go(pathTeachers, pathClasses);
    }

    private void go(String teachers, String classes) {
        List<SchoolTeacher> schoolTeachers = new ArrayList<SchoolTeacher>();
        String[][] schoolSchedule = new String[50][50];
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader(teachers));
            int countOfTeachers = sc.nextInt();
            for (int i = 0; i < countOfTeachers; i++) {
                schoolTeachers.add(i, new SchoolTeacher());
                String s = sc.next();
                schoolTeachers.get(i).setName(sc.next());
                while (!sc.hasNextInt()) {
                    if (sc.hasNext()) {
                        schoolTeachers.get(i).setSubjects(sc.next());
                    } else {
                        break;
                    }
                }
            }

            List<SchoolClass> schoolClasses = new ArrayList<SchoolClass>();
            sc = new Scanner(new FileReader(classes));
            int countOfClasses = sc.nextInt();
            for (int i = 0; i < countOfClasses; i++) {
                schoolClasses.add(i, new SchoolClass());
                String s = sc.next();
                schoolClasses.get(i).setName(sc.next());
                while (!sc.hasNextInt()) {
                    if (sc.hasNext()) {
                        schoolClasses.get(i).setSubjectsAndCount(sc.next(), sc.nextInt());
                    } else {
                        break;
                    }
                }
            }

            int count;
            do {
                count = 0;
                for (int i = 1; i < schoolClasses.size(); i++) {
                    if (schoolClasses.get(i - 1).max > schoolClasses.get(i).max) {
                        SchoolClass schoolClass = schoolClasses.get(i - 1);
                        schoolClasses.set(i - 1, schoolClasses.get(i));
                        schoolClasses.set(i, schoolClass);
                        count++;
                    }

                }
            } while (count != 0);

            for (int i = 0; i < countOfClasses; i++) {
                schoolClasses.get(i).createSubjectAndTeacher(schoolTeachers);
                System.out.println(schoolClasses.get(i).getSubjectAndTeacher());
            }

            for (int i = 0; i < 5; i++) {  //days
                for (int l = 0; l < schoolTeachers.size(); l++) {
                    for (int j = 0; j < schoolTeachers.get(l).empty.size(); j++) {
                        schoolTeachers.get(l).empty.set(j, false);
                    }
                }

                int stroka = 0;
                int flag = 0;
                int k1;

                for (int j = 0; j < schoolClasses.size(); j++) { //classes
                    int[] maxIndex = getMaxIndexes(schoolClasses.get(j).countOfSubjects);
                    for (int k = 0; k < maxIndex.length; k++) { //lessons
                        for (int l = 0; l < schoolTeachers.size(); l++) {
                            if (schoolTeachers.get(l).getName().equals(schoolClasses.get(j).teachers.get(maxIndex[k]))) {
                                k1 = k;
                                boolean trues = true;
                                List<Integer> fixMaxIndexes = new ArrayList<Integer>(schoolClasses.get(j).countOfSubjects);
                                while (trues) {
                                    if (!schoolTeachers.get(l).empty.get(k) && schoolSchedule[k + i * 6][j] == null) {
                                        schoolTeachers.get(l).empty.set(k, true);
                                        schoolClasses.get(j).countOfSubjects.set(maxIndex[k1], schoolClasses.get(j).countOfSubjects.get(maxIndex[k1]) - 1);
                                        schoolSchedule[k + i * 6][j] = schoolClasses.get(j).getSubjectAndTeacher().get(schoolClasses.get(j).subjects.get(maxIndex[k1])) + " " + schoolClasses.get(j).subjects.get(maxIndex[k1]);
                                        flag++;
                                        break;
                                    } else {
                                        if (k == maxIndex.length - 1) {
                                            k = 0;
                                        } else {
                                            k++;
                                        }
                                        stroka++;
                                        if (stroka == 6) {
                                            fixMaxIndexes.remove(k1);
                                            maxIndex = getMaxIndexes(fixMaxIndexes);
                                            stroka = 0;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (flag != 0) {
                                flag = 0;
                                break;
                            }
                        }
                        while (stroka != 0) {
                            if (k == 0) {
                                k = maxIndex.length - 1;
                            } else {
                                k--;
                            }
                            stroka--;
                        }
                    }
                }

            }
            table.setValueAt("Понедельник", 0, 0);
            table.setValueAt("Вторник", 7, 0);
            table.setValueAt("Среда", 14, 0);
            table.setValueAt("Четверг", 21, 0);
            table.setValueAt("Пятница", 28, 0);
            int i1 = 0;
            for (int i = 0; i < 35; i++) {
                if (i % 7 == 0){
                    i1 --;
                }
                for (int j = 0; j < countOfClasses; j++) {
                    if (i % 7 == 0) {
                        table.setValueAt(" ", i + 1, j + 1);
                    } else {
                        table.setValueAt(schoolClasses.get(j).getName(), 0, j + 1);
                        table.setValueAt(schoolSchedule[i1][j], i, j + 1);
                    }
                }
                i1++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } finally {
            sc.close();
        }
    }

    private static int[] getMaxIndexes(List<Integer> mass) {
        List<Integer> mass1 = new ArrayList<Integer>(mass);
        int[] index = new int[6];

        for (int i = 0; i < 6; i++) {
            int maxElement = mass1.get(0);

            for (int j = 0; j < mass1.size(); j++) {
                if (mass1.get(j) > maxElement) {
                    maxElement = mass1.get(j);
                    index[i] = j;
                }
            }
            mass1.set(index[i], -1);
        }

        return index;
    }

}
