package by.gsu.coursework;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Romanyuk Dmitriy.
 */
public class SchoolTeacher {
    private String name;
    private List<String> subjects = new ArrayList<String>();
    public List<Boolean> empty = new ArrayList<Boolean>();

    public SchoolTeacher() {
        for (int i = 0; i < 6; i++) {
            empty.add(false);
        }
    }


    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(String subject) {
        subjects.add(subject);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
