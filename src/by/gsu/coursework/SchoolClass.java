package by.gsu.coursework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Romanyuk Dmitriy.
 */
public class SchoolClass {
    private String name;
    private Map<String, Integer> subjectsAndCount = new HashMap<String, Integer>();
    private Map<String, String> subjectAndTeacher = new HashMap<String, String>();
    public List<String> subjects = new ArrayList<String>();
    public List<Integer> countOfSubjects = new ArrayList<Integer>();
    public List<String> teachers = new ArrayList<String>();
    public int max = 0;


    public void createSubjectAndTeacher(List<SchoolTeacher> schoolTeachers) {
        for (int i = 0; i < subjects.size(); i++) {
            teachers.add(null);
        }

        for (Map.Entry subjectClass :
                subjectsAndCount.entrySet()) {

            for (SchoolTeacher teacher :
                    schoolTeachers) {
                for (String subjectTeacher :
                        teacher.getSubjects()) {
                    if (subjectTeacher.equals(subjectClass.getKey())) {
                        subjectAndTeacher.put((String) subjectClass.getKey(), teacher.getName());
                        for (int i = 0; i < subjects.size(); i++) {
                            if (subjects.get(i).equals(subjectClass.getKey())) {
                                teachers.set(i, teacher.getName());
                            }
                        }

                    }
                }
            }

        }
    }

    public Map<String, Integer> getSubjectsAndCount() {
        return subjectsAndCount;
    }

    public void setSubjectsAndCount(String key, Integer value) {
        subjectsAndCount.put(key, value);
        subjects.add(key);
        countOfSubjects.add(value);
        if (value > max)
            max = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getSubjectAndTeacher() {
        return subjectAndTeacher;
    }
}
